package org.runite.client;

import com.jogamp.opengl.*;
import java.nio.ByteBuffer;

final class Class156 {

    private final int anInt1992;
    private final boolean aBoolean1994;
    private int anInt1991;
    private int anInt1993;


    public Class156() {
        this(false);
    }

    Class156(boolean var1) {
        this.anInt1991 = -1;
        this.anInt1993 = 0;
        GL2 var2 = HDToolKit.gl;
        int[] var3 = new int[1];
        var2.glGenBuffers(1, var3, 0);
        this.aBoolean1994 = var1;
        this.anInt1991 = var3[0];
        this.anInt1992 = Class31.anInt582;
    }

    final void method2168(ByteBuffer var1) {
        if (var1.limit() <= this.anInt1993) {
            GL2 var2 = HDToolKit.gl;
            var2.glBindBuffer(0x8892, this.anInt1991);
            var2.glBufferSubData(0x8892, 0, var1.limit(), var1);
        } else {
            this.method2172(var1);
        }

    }

    protected final void finalize() throws Throwable {
        if (this.anInt1991 != -1) {
            Class31.method989(this.anInt1991, this.anInt1993, this.anInt1992);
            this.anInt1991 = -1;
            this.anInt1993 = 0;
        }

        super.finalize();
    }

    final void method2169() {
        GL2 var1 = HDToolKit.gl;
        var1.glBindBuffer(0x8892, this.anInt1991);
    }

    final void method2170(ByteBuffer var1) {
        GL2 var2 = HDToolKit.gl;
        var2.glBindBuffer(GL2.GL_ELEMENT_ARRAY_BUFFER, this.anInt1991);
        var2.glBufferData(GL2.GL_ELEMENT_ARRAY_BUFFER, var1.limit(), var1, this.aBoolean1994 ? 0x88e0 : 0x88e4);
        Class31.anInt585 += var1.limit() - this.anInt1993;
        this.anInt1993 = var1.limit();
    }

    final void method2171() {
        GL2 var1 = HDToolKit.gl;
        var1.glBindBuffer(GL2.GL_ELEMENT_ARRAY_BUFFER, this.anInt1991);
    }

    final void method2172(ByteBuffer var1) {
        GL2 var2 = HDToolKit.gl;
        var2.glBindBuffer(0x8892, this.anInt1991);
        var2.glBufferData(0x8892, var1.limit(), var1, this.aBoolean1994 ? 0x88e0 : 0x88e4);
        Class31.anInt585 += var1.limit() - this.anInt1993;
        this.anInt1993 = var1.limit();
    }
}
